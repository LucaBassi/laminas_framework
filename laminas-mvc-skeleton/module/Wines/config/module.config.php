<?php

namespace Wines;
use Laminas\Router\Http\Segment;
return [

    'router' => [
        'routes' => [
            'wines' => [
                'type' => Segment::class,
                'options' => [
                    'route' => '/wines[/:action[/:id]]',
                    'constraints' => [
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id' => '[0-9]+',
                    ],
                    'defaults' => [
                        'controller' => Controller\WinesController::class,
                        'action' => 'index',
                    ],
                ],
            ],
        ],
    ],
    'view_manager' => [
        'template_path_stack' => [
            'post' => __DIR__ . '/../view',
        ],
    ],
];
