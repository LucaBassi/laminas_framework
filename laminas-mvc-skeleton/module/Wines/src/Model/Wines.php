<?php
namespace Wines\Model;

class Wines
{
    public $code;
    public $brand;
    public $model;

    public function exchangeArray(array $data)
    {
        $this->code     = !empty($data['code']) ? $data['code'] : null;
        $this->brand = !empty($data['brand']) ? $data['brand'] : null;
        $this->model  = !empty($data['model']) ? $data['model'] : null;
        $this->photo  = !empty($data['photo']) ? $data['photo'] : null;
    }
} 