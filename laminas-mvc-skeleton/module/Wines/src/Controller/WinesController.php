<?php

namespace Wines\Controller;

// Add the following import:
use Wines\Model\WinesTable;
use Laminas\Mvc\Controller\AbstractActionController;
use Laminas\View\Model\ViewModel;

class WinesController extends AbstractActionController
{
    // Add this property:
    private $table;

    // Add this constructor:
    public function __construct(WinesTable $table)
    {
        $this->table = $table;
    }
    public function indexAction()
    {
         return new ViewModel([
            'winess' => $this->table->fetchAll(),
        ]);
      
    }

    public function addAction()
    {
    }

    public function editAction()
    {
    }

    public function deleteAction()
    {
    }
}