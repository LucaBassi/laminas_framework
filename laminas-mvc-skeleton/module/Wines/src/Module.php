<?php
namespace Wines;

// Add these import statements:
use Laminas\Db\Adapter\AdapterInterface;
use Laminas\Db\ResultSet\ResultSet;
use Laminas\Db\TableGateway\TableGateway;
use Laminas\ModuleManager\Feature\ConfigProviderInterface;

class Module implements ConfigProviderInterface
{
    // getConfig() method is here
    public function getConfig()
    {
        return include __DIR__ . '/../config/module.config.php';
    }
    // Add this method:
    public function getServiceConfig()
    {
        return [
            'factories' => [
                Model\WinesTable::class => function($container) {
                    $tableGateway = $container->get(Model\WinesTableGateway::class);
                    return new Model\WinesTable($tableGateway);
                },
                Model\WinesTableGateway::class => function ($container) {
                    $dbAdapter = $container->get(AdapterInterface::class);
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new Model\Wines());
                    return new TableGateway('wines' , $dbAdapter, null, $resultSetPrototype);
                },
            ],
        ];
    }
    
     public function getControllerConfig()
    {
        return [
            'factories' => [
                Controller\WinesController::class => function($container) {
                    return new Controller\WinesController(
                        $container->get(Model\WinesTable::class)
                    );
                },
            ],
        ];
    }
}
