<?php
namespace Meteo\Model;

class Meteo
{
    public $id;
    public $name;
    public $slug;

    public function exchangeArray(array $data)
    {
        $this->id     = !empty($data['id']) ? $data['id'] : null;
        $this->name = !empty($data['name']) ? $data['name'] : null;
        $this->slug  = !empty($data['slug']) ? $data['slug'] : null;
    }
} 