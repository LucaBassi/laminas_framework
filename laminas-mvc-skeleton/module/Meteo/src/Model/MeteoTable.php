<?php
namespace Meteo\Model;

use RuntimeException;
use Laminas\Db\TableGateway\TableGatewayInterface;

class MeteoTable
{
    private $tableGateway;

    public function __construct(TableGatewayInterface $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function fetchAll()
    {
        return $this->tableGateway->select();
    }

    public function getMeteo($id)
    {
        $id = (int) $id;
        $rowset = $this->tableGateway->select(['id' => $id]);
        $row = $rowset->current();
        if (! $row) {
            throw new RuntimeException(sprintf(
                'Could not find row with identifier %d',
                $id
            ));
        }

        return $row;
    }

    public function saveMeteo(Meteo $Meteo)
    {
        $data = [
            'name' => $Meteo->name,
            'slug'  => $Meteo->slug,
        ];

        $id = (int) $Meteo->id;

        if ($id === 0) {
            $this->tableGateway->insert($data);
            return;
        }

        try {
            $this->getMeteo($id);
        } catch (RuntimeException $e) {
            throw new RuntimeException(sprintf(
                'Cannot update Meteo with identifier %d; does not exist',
                $id
            ));
        }

        $this->tableGateway->update($data, ['id' => $id]);
    }

    public function deleteMeteo($id)
    {
        $this->tableGateway->delete(['id' => (int) $id]);
    }
}