<?php
namespace Meteo;

// Add these import statements:
use Laminas\Db\Adapter\AdapterInterface;
use Laminas\Db\ResultSet\ResultSet;
use Laminas\Db\TableGateway\TableGateway;
use Laminas\ModuleManager\Feature\ConfigProviderInterface;

class Module implements ConfigProviderInterface
{
    // getConfig() method is here
    public function getConfig()
    {
        return include __DIR__ . '/../config/module.config.php';
    }
    // Add this method:
    public function getServiceConfig()
    {
        return [
            'factories' => [
                Model\MeteoTable::class => function($container) {
                    $tableGateway = $container->get(Model\MeteoTableGateway::class);
                    return new Model\MeteoTable($tableGateway);
                },
                Model\MeteoTableGateway::class => function ($container) {
                    $dbAdapter = $container->get(AdapterInterface::class);
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new Model\Meteo());
                    return new TableGateway('meteo' , $dbAdapter, null, $resultSetPrototype);
                },
            ],
        ];
    }
    
     public function getControllerConfig()
    {
        return [
            'factories' => [
                Controller\MeteoController::class => function($container) {
                    return new Controller\MeteoController(
                        $container->get(Model\MeteoTable::class)
                    );
                },
            ],
        ];
    }
}
