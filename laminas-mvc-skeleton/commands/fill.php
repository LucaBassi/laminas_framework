<?php

require dirname(__DIR__) . '/vendor/autoload.php';

$faker = Faker\Factory::create('fr_FR');

$pdo = new PDO('mysql:dbname=laminastutorial;host=127.0.0.1', 'root', "", [PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION]);

$pdo->exec("CREATE TABLE album (id INTEGER PRIMARY KEY AUTOINCREMENT, artist varchar(100) NOT NULL, title varchar(100) NOT NULL);");


$pdo->exec("CREATE TABLE post (
    id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
    name varchar(100),
    slug varchar(500),
    created_at varchar(100),
    content varchar(500)
); ");

$pdo->exec("CREATE TABLE category (
    id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
    name varchar(100),
    slug varchar(500)
); ");

$pdo->exec("CREATE TABLE post_category (
    id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
    post_id int,
    category_id int
); ");
$pdo->exec("CREATE TABLE user (
    id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
    username varchar(100),
    password varchar(100)
); ");
$pdo->exec('SET FOREIGN_KEY_CHECKS = 0');
$pdo->exec('TRUNCATE TABLE post_category');
$pdo->exec('TRUNCATE TABLE post');
$pdo->exec('TRUNCATE TABLE category');
$pdo->exec('TRUNCATE TABLE user');
$pdo->exec('SET FOREIGN_KEY_CHECKS = 1');
$posts = [];
$categories = [];
for ($i = 0; $i < 50; $i++) {
    $pdo->exec("insert into post SET name ='{$faker->sentence()}' ,slug='{$faker->slug}', created_at='{$faker->iso8601}' , content='{$faker->paragraphs(rand(2, 15), $asText = true) }'");
    $posts[] = $pdo->lastInsertId();
}

for ($i = 0; $i < 50; $i++) {
    $pdo->exec("insert into category SET name ='{$faker->sentence(3)}' ,slug='{$faker->slug}'");

    $categories[] = $pdo->lastInsertId();
}

foreach ($posts as $post) {
    $randomCategories = $faker->randomElements($categories, rand(0, count($categories)));
    foreach ($randomCategories as $category) {
        $pdo->exec("INSERT INTO post_category SET post_id=$post , category_id =$category");
    }
}
$password = password_hash('admin', PASSWORD_BCRYPT);
$pdo->exec("INSERT INTO user SET username = 'admin' ,password='$password'");
